FROM composer

FROM php:8.3-fpm-alpine AS base

ENV APCU_VERSION 5.1.21

COPY --from=composer /usr/bin/composer /usr/local/bin/composer

RUN apk add --no-cache \
        linux-headers \
        bash \
        git \
        icu-libs \
        unzip \
        libpq-dev \
        libzip-dev \
        zlib-dev && \
    apk add --no-cache --virtual .build-deps \
        $PHPIZE_DEPS \
        icu-dev && \
    docker-php-ext-install \
        intl \
        zip && \
    pecl install apcu-${APCU_VERSION} && \
    pecl install -f xdebug && \
    docker-php-ext-enable apcu && \
    docker-php-ext-enable xdebug && \
    docker-php-ext-enable opcache && \
    docker-php-ext-install pcntl && \
    apk del .build-deps && \
    apk add gosu --update --no-cache --repository http://dl-3.alpinelinux.org/alpine/edge/testing/ --allow-untrusted && \
    addgroup dev && \
    adduser -D -h /home -s /bin/sh -G dev dev

###> recipes ###
###> doctrine/doctrine-bundle ###
RUN apk add --no-cache --virtual .pgsql-deps postgresql-dev; \
    docker-php-ext-install -j$(nproc) pdo_pgsql; \
    apk add --no-cache --virtual .pgsql-rundeps so:libpq.so.5; \
    apk del .pgsql-deps
###< doctrine/doctrine-bundle ###
###> symfony/panther ###
# Chromium and ChromeDriver
ENV PANTHER_NO_SANDBOX 1
# Not mandatory, but recommended
ENV PANTHER_CHROME_ARGUMENTS='--disable-dev-shm-usage'
RUN apk add --no-cache chromium chromium-chromedriver

# Firefox and geckodriver
ARG GECKODRIVER_VERSION=0.29.0
RUN apk add --no-cache firefox
RUN wget -q https://github.com/mozilla/geckodriver/releases/download/v$GECKODRIVER_VERSION/geckodriver-v$GECKODRIVER_VERSION-linux64.tar.gz; \
	tar -zxf geckodriver-v$GECKODRIVER_VERSION-linux64.tar.gz -C /usr/bin; \
	rm geckodriver-v$GECKODRIVER_VERSION-linux64.tar.gz
###< symfony/panther ###
###< recipes ###

ADD docker/php/entrypoint.sh /entrypoint
RUN chmod +x /entrypoint

ENTRYPOINT ["/entrypoint"]

FROM base AS dev

COPY --link docker/php/conf.d/php.ini $PHP_INI_DIR/php.ini
COPY --link docker/php/conf.d/symfony.ini $PHP_INI_DIR/conf.d/
COPY --link docker/php/conf.d/99-xdebug.ini $PHP_INI_DIR/conf.d/

FROM base AS ci

COPY --link docker/php/conf.d/php.ini $PHP_INI_DIR/php.ini
COPY --link docker/php/conf.d/symfony.ini $PHP_INI_DIR/conf.d/
COPY --link docker/php/conf.d/ci/99-xdebug-ci.ini $PHP_INI_DIR/conf.d/
