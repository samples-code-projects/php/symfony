<?php

declare(strict_types=1);

namespace App\Tests\E2E;

use Symfony\Component\Panther\PantherTestCase;
use Zenstruck\Foundry\Test\Factories;

class AbstractPantherTestCase extends PantherTestCase
{
    use Factories;
}
