DOCKER_COMPOSE  = $(or docker compose, docker-compose)

EXEC_PHP        = $(DOCKER_COMPOSE) exec -T php /entrypoint
RUN_QA          = $(DOCKER_COMPOSE) run --rm qa

XDEBUG_OFF		= php -d xdebug.mode=off
PHP				= $(EXEC_PHP) $(XDEBUG_OFF)
COMPOSER        = $(EXEC_PHP) composer
CONSOLE			= $(PHP) bin/console
PHPUNIT			= $(PHP) bin/phpunit
TWIG_CS_FIXER	= $(RUN_QA) twig-cs-fixer
PHP_CS_FIXER	= $(RUN_QA) php-cs-fixer
PHPSTAN			= $(RUN_QA) phpstan

##   _____                  __
##  / ____|                / _|
## | (___  _   _ _ __ ___ | |_ ___  _ __  _   _
##  \___ \| | | | '_ ` _ \|  _/ _ \| '_ \| | | |
##  ____) | |_| | | | | | | || (_) | | | | |_| |
## |_____/ \__, |_| |_| |_|_| \___/|_| |_|\__, |
##          __/ |                          __/ |
##         |___/                          |___/

##
##Setup 🐳
##--------

install: ## Install the project
install: build start vendor secrets-decrypt-to-local db
.PHONY: install

clean: ## Remove Docker containers and delete generated files/folders
clean: kill
	rm -rf .env.*.local var vendor assets/vendor .phpunit.result.cache
.PHONY: clean

restart: ## Restart Docker containers
restart: stop start
.PHONY: restart

reset: ## Clean and install the project
reset: clean install
.PHONY: reset

build:
	$(DOCKER_COMPOSE) build
.PHONY: build

start:
	$(DOCKER_COMPOSE) up -d --remove-orphans --no-recreate $(services)
.PHONY: start

stop:
	$(DOCKER_COMPOSE) stop $(services)
.PHONY: stop

kill:
	$(DOCKER_COMPOSE) kill
	$(DOCKER_COMPOSE) down --volumes --remove-orphans
.PHONY: kill

##
##Symfony 🚀
##----------

sf: ## List Symfony commands. You can also run Symfony commands (e.g. make sf command='about')
	$(CONSOLE) $(or $(command), list)
.PHONY: sf

db: ## Reset the dev database, apply the migrations and load the fixtures
db: db-reset db-migrations db-fixtures
.PHONY: db

db-reset: ## Drop and create database
	$(CONSOLE) doctrine:database:drop --if-exists --force --env=$(or $(env), dev)
	$(CONSOLE) doctrine:database:create --env=$(or $(env), dev)
.PHONY: db-reset

db-migrations: ## Apply database migrations
	$(CONSOLE) doctrine:migrations:migrate --no-interaction --allow-no-migration --env=$(or $(env), dev)
.PHONY: db-migrations

db-fixtures: ## Load database fixtures
	$(CONSOLE) doctrine:fixtures:load --no-interaction --purge-with-truncate --env=$(or $(env), dev)
.PHONY: db-fixtures

migration: ## Generate a new doctrine migration
	$(CONSOLE) doctrine:migrations:diff
.PHONY: migration

secrets-decrypt-to-local: ## Write secrets in env local file (e.g. make secrets-decrypt-to-local env='dev')
	$(CONSOLE) secrets:decrypt-to-local --env=$(or $(env), dev) --force
.PHONY: secrets-decrypt-to-local

##
##Composer 🛠️
##-----------

composer: ## List Composer commands. You can also run Composer commands (e.g. make composer command='--version')
	$(COMPOSER) $(or $(command), list)
.PHONY: composer

composer-audit: ## Check vulnerability in PHP packages
	$(COMPOSER) audit
.PHONY: composer-audit

vendor: composer.lock
	$(COMPOSER) install -n --prefer-dist
.PHONY: vendor

##
##Tests 🧪
##--------

tests: ## Run all tests
tests: tests-unit tests-integration tests-functional tests-e2e
.PHONY: tests

tests-unit: ## Run unit tests
	$(PHPUNIT) --testsuite=unit
.PHONY: tests-unit

tests-integration: ## Run integration tests
	$(PHPUNIT) --testsuite=integration
.PHONY: tests-integration

tests-functional: ## Run functional tests
	$(PHPUNIT) --testsuite=functional
.PHONY: tests-functional

tests-e2e: ## Run e2e tests
	$(PHPUNIT) --testsuite=e2e
.PHONY: tests-e2e

secrets-decrypt-to-local-test: ## Write secrets in .env.test.local file
secrets-decrypt-to-local-test: env=test
secrets-decrypt-to-local-test: secrets-decrypt-to-local
.PHONY: secrets-decrypt-to-local-test

##
##QA 🐛
##-----

twig-cs-fixer-dry: ## Twig-CS-Fixer (https://github.com/VincentLanglet/Twig-CS-Fixer)
	$(TWIG_CS_FIXER) lint
.PHONY: twig-cs-fixer-dry

twig-cs-fixer-apply: ## Apply Twig-CS-Fixer fixes
	$(TWIG_CS_FIXER) lint --fix
.PHONY: twig-cs-fixer-apply

php-cs-fixer-dry: ## PHP-CS-Fixer (https://cs.symfony.com)
	$(PHP_CS_FIXER) fix --dry-run --verbose --diff
.PHONY: php-cs-fixer-dry

php-cs-fixer-apply: ## Apply PHP-CS-Fixer fixes
	$(PHP_CS_FIXER) fix --verbose --diff
.PHONY: php-cs-fixer-apply

phpstan: ## PHPStan (https://phpstan.org)
	$(PHPSTAN) analyse
.PHONY: phpstan

##
##CI 👮
##-----

install-ci: ## Install the project for CI
install-ci: start vendor secrets-decrypt-to-local-test
.PHONY: install-ci

ci: ## Run CI tasks
ci: twig-cs-fixer-dry php-cs-fixer-dry phpstan secrets-decrypt-to-local-test tests
.PHONY: ci

##
##Shell 🐚
##--------

sh: ## Open a shell in a Docker Compose service (e.g. make sh service='php')
	$(DOCKER_COMPOSE) exec $(or $(service), php) sh
.PHONY: sh

##
##Logs 📋
##-------

logs: ## Display Docker Compose service logs (e.g. make logs service='php')
	$(DOCKER_COMPOSE) logs -f $(or $(service), php)
.PHONY: logs

##
##Help ℹ️
##-------

help: ## List Makefile commands
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
.PHONY: help

.DEFAULT_GOAL := help
