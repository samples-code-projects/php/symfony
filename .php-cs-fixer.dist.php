<?php

$finder = PhpCsFixer\Finder::create()
    ->in(__DIR__)
    ->exclude('var')
    ->exclude('vendor')
    ->exclude('config')
    ->notPath('src/Kernel.php')
    ->notPath('tests/bootstrap.php')
    ->notPath('public/index.php')
    ->notPath('importmap.php')
;

$config = new PhpCsFixer\Config();
return $config->setRules([
    '@Symfony' => true,
    'declare_strict_types' => true,
    'array_syntax' => ['syntax' => 'short'],
])
    ->setRiskyAllowed(true)
    ->setUsingCache(false)
    ->setFinder($finder)
;
