#!/bin/sh

username='dev'
group='dev'

uid=$(stat -c %u /srv)
gid=$(stat -c %g /srv)

if [ $uid == 0 ] && [ $gid == 0 ]; then
    if [ $# -eq 0 ]; then
        php-fpm --allow-to-run-as-root
    else
        echo "$@"
        exec "$@"
    fi
fi

sed -i -r "s/${username}:x:\d+:\d+:/${username}:x:$uid:$gid:/g" /etc/passwd
sed -i -r "s/${group}:x:\d+:/${group}:x:$gid:/g" /etc/group

sed -i "s/user = www-data/user = ${username}/g" /usr/local/etc/php-fpm.d/www.conf
sed -i "s/group = www-data/group = ${group}/g" /usr/local/etc/php-fpm.d/www.conf

user=$(grep ":x:$uid:" /etc/passwd | cut -d: -f1)
if [ $# -eq 0 ]; then
    php-fpm
else
    exec gosu $user "$@"
fi
