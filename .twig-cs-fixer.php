<?php

$ruleset = new TwigCsFixer\Ruleset\Ruleset();

$config = new TwigCsFixer\Config\Config();
$config->setRuleset($ruleset);
$config->setCacheFile(null);

return $config;
